/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwords;

/**
 *
 * @author alumneDAM
 */
public class Passwords {

        long longitud=6;
        int password;

    public Passwords(int password) {
        this.password = password;
    }

    public long getLongitud() {
        return longitud;
    }

    public void setLongitud(long longitud) {
        if(longitud>6&&longitud<11){
            this.longitud = longitud;
        }
        
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }
    
    
}
